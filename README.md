SAGE2 for the Stanford HIVE
===========================

The HIVE fork of SAGE2 is a slightly customized version of SAGE2 for the HIVE wall at Stanford. This fork includes the TOP/DOMDEC Viewer as a submodule via its [GitHub repository](https://github.com/MatejJan/SAGE2-TOP-DOMDEC-Viewer).

All information about SAGE2 is in the original repository:
https://bitbucket.org/sage2/sage2
